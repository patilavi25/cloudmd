public class PatientSummary {
    public user currentuser{get;set;}
    public Id patientId {get;set;}
    public Contact contact {get;set;}
    public Id consultationId  = apexPages.currentPage().getParameters().get('consultationId');
    public Id visitId {get;set;}
    public string patientName {get;set;}
    public string patientGender {get;set;}
    public date patientDOB {get;set;}
    public string email {get;set;}
    public Decimal patientAge {get;set;}
    public List<Patient_Allergy__c> allergies;
    public Diagnosis__c diagnos {get;set;}
    // Medicine
    Public Patient_Medicine__c medicine {get;set;}
    Public  Medicine_List__c medicinelist {get;set;} 
    //Close Medicine
    public List<Patient_Diagnosis__c> patientDiagnosis;
    public List<Patient_Diagnosis__c> patientProblemList;
    public List<Patient_Medicine__c> patientMedication;
    public List<Lab_Report__c> patientlabData;
    public List<Vitals__c> patientVital;
    public List<Attachment> patientDocuments;
    public List<Contact> patientInfo {get;set;}
    public List<SOAP_Note__c> patientSOAPNotes {get;set;}
    private final Contact con;
    public string selectedSOAPId {get;set;}
    public string errorMessage {get;set;}
    SOAP_Note__c SOAPNote;
    public string jsonDataSOAPRecord {get;set;}
    public List<Diagnosis__c> parentDiagnosis {get;set;}
    public boolean displayAddPopup {get; set;}
    //Medicine
    public boolean displayAddPopupMedicine {get; set;} 
    //Close Medicine
	
    public List<MedicineListWrapper> mlWrapper {get;set;}
    Public  List<Patient_Medicine__c> selectedPatientMedicineList {get;set;}
    public String  Licensenumber {get;set;}
    public String  signature {get;set;}
    public String  watermarkURL {get;set;}
    public String  signatureOnDetailpage {get;set;}
    public String  watermarkURLOnDetailpage {get;set;}
    
    public PatientSummary() {
        
        String currentUrl = URL.getCurrentRequestUrl().toExternalForm();
        system.debug('currentUrl: '+currentUrl);
        if(currentUrl.contains('PatientSummaryPage')){
            system.debug('-In first method-');
            selectedPatientMedicineList = new List<Patient_Medicine__c>();
        }
        
        //this.con = (Contact)stdController.getRecord();
        patientId  = apexPages.currentPage().getParameters().get('Id');
        contact = [Select Name, PrematureCAD_55__c, Hypertension__c, ThyroidCancer__c, Prostate_Cancer__c, AlcoholUse__c, Drug_Abuse__c, SmokingHistory__c, ColonCancer__c, Ovarian_Cancer__c, LungCancer__c, BreastCancer__c, Dyslipidemia__c, Diabetes__c, Gender__c, Age__c, Email, Birthdate from Contact where Id=: patientId limit 1];        
        patientInfo = [Select Name, Gender__c, Age__c, Email, Birthdate from Contact where Id=: patientId ];
        patientName = patientInfo[0].Name;
        patientGender = patientInfo[0].Gender__c;
        patientDOB = patientInfo[0].Birthdate;
        patientAge = patientInfo[0].Age__c;
        email = patientInfo[0].Email;
        system.debug('Patient Id - '+patientId);
        List<Visit__c> visit1 = new List<Visit__c>([Select Id, Patient__c from Visit__c]);
        system.debug('Visit Id - '+visit1);
        if(patientId != null) {
            List<Visit__c> visit = new List<Visit__c>([Select Id from Visit__c where Patient__c =: patientId Order By CreatedDate desc Limit 1 ]);
            system.debug('Visit Id - '+visit[0].Id);
            visitId = visit[0].Id;
        }
        currentuser=new User();
     	currentuser=[Select Id,Name,Email, signature__c, License_number__c from User where Id=:userinfo.getuserId()];
        List <Document> documentList = [SELECT Id, Name, DeveloperName, isDeleted, Body, ContentType, Type, IsPublic, AuthorId, FolderId from Document WHERE Name= 'Watermark Image' AND isDeleted=false limit 1];
        if(documentList.size()>0){
            watermarkURLOnDetailpage = URL.getSalesforceBaseUrl().toExternalForm() +'/servlet/servlet.FileDownload?file=' + documentList[0].Id;
            system.debug('--watermarkURLOnDetailpage--'+watermarkURLOnDetailpage);
            watermarkURL = '/servlet/servlet.FileDownload?file=' + documentList[0].Id;
        }
        system.debug('--watermarkURL--'+watermarkURL);
        List<Physician_Info__c> physicianinfo = [Select Id, Physician__c, SignatureURL__c, WatermarkImageURL__c From Physician_Info__c where Physician__c=:userinfo.getuserId() limit 1];
        if(physicianinfo.size()>0){
            signature = physicianinfo[0].SignatureURL__c.substringAfter('.com');
            signatureOnDetailpage = physicianinfo[0].SignatureURL__c;
            system.debug('--signatureOnDetailpage--'+signatureOnDetailpage);
        }else{
            if(currentuser.signature__c != null && currentuser.signature__c !=''){
                signature = currentuser.signature__c.substringAfter('.com');
                signatureOnDetailpage = currentuser.signature__c;
            }
            
        }           
        system.debug('--mlWrapper--'+mlWrapper+' ---selectedPatientMedicineList---'+selectedPatientMedicineList);
    }
    
    public String SOAPNoteId {get; set;}
    public SOAP_Note__c SOAPNoteObj {get; set;}
    public boolean displayPopup {get; set;}     
    
    public boolean render_on_add {get; set;} 
   
    //Medicine 
    public boolean render_on_add_Medicine {get; set;} 
    //Close Medicine

    public void closePopup() {        
        displayPopup = false;
        SOAPNoteId = null;
        SOAPNoteObj = null;  
    }
    public void closeAddPopup() { 
        system.debug('displayAddPopup '+ displayAddPopup);
    	displayAddPopup = false;
               
        render_on_add = false;
        diagnos = null;
    }
    
    //Medicine Pop up
    public void closeAddPopupMedicine() { 
        system.debug('displayAddPopupMedicine '+ displayAddPopupMedicine);
    	displayAddPopupMedicine = false;
        render_on_add_Medicine = false;
        medicine = null;
    }
    //Close Medicine 
    
    public void showPopup() { 
        if(SOAPNoteId != null) {
            SOAPNoteObj = [Select id, name, Soap_Assessment__c, Soap_Objective__c, Provider__c, Soap_Plan__c, Subjective__c, Visit__c from SOAP_Note__c where Id = :SOAPNoteId];
        } 
        else {
            SOAPNoteObj = new SOAP_Note__c();
        }   
        displayPopup = true;    
    }
    
    public void addPopup() { 
       if(diagnos == null) diagnos = new Diagnosis__c();
        diagnos.Name = null;
        diagnos.Description__c = null;
        
           //elect id, Name, Description__c from Diagnosis__c limit 50];
        displayAddPopup = true;    
    }
    // Medicine Pop up 
     public void addPopupMedicine() { 
       if(medicine == null) medicine = new Patient_Medicine__c();
       	medicine.Medicine_List__c = null;
        medicine.Route_of_Administration__c = null;
        medicine.Dose__c = null;
        medicine.Frequency__c=null;
        medicine.Injection_doses__c = null;
        medicine.Duration__c = null;
        medicine.Refill__c = null;
        displayAddPopupMedicine = true;     
    }
    //clode Medicine
    
    public List<Diagnosis__c> parentDiagnosis(){
        return [select id, Name, Description__c from Diagnosis__c limit 50];
    }
    /**
     * function triggers on save of Active Problem List add button
	**/
    public void saveDiagonosisPopup() {
        system.debug('one - 70' + JSON.serialize(diagnos));
        if(diagnos != null){ 
            system.debug('if one - 70');
        // to insert create new object
       // Diagnosis__c diagnos = new Diagnosis__c();
       // diagnos.Name = diagnosisName;
       // diagnos.Description__c = diagnosisDescription;
        errorMessage = null;
            try {
                insert diagnos;
            } catch(DmlException e) {
                ApexPages.addMessages(e);
                errorMessage = 'Duplicate found in Diagnosis';
                //displayAddPopup = true;
            }
        Patient_Diagnosis__c patientDiagnos = new Patient_Diagnosis__c();
        patientDiagnos.Diagnosis__c = diagnos.Id;
        patientDiagnos.Patient__c = patientId;
        patientDiagnos.Active__c = true;
            try { 
                insert patientDiagnos;
            } catch(DmlException e) {
                ApexPages.addMessages(e); 
                errorMessage = 'Duplicate found in Patient Diagnosis';
              //  displayAddPopup = true;
            }
        displayAddPopup = false;
        }
        system.debug('one - 70 '+ displayAddPopup);
    }
    //Medicine save
     public void savePopupMedicine() {
        system.debug('one - 70' + JSON.serialize(medicine));
        if(medicine != null){ 
            system.debug('if one - 70');
        // to insert create new object
       // Diagnosis__c diagnos = new Diagnosis__c();
       // diagnos.Name = diagnosisName;
       // diagnos.Description__c = diagnosisDescription;
        errorMessage = null;
            try {
                medicine.Patient__c = patientId;
                insert medicine;
                medicine = null;
            } catch(DmlException e) {
                ApexPages.addMessages(e);
                System.debug('Errorrr++++'+e.getMessage());
                System.debug('Errorrr++++'+e.getStackTraceString());
                errorMessage = 'Duplicate found in medicine';
                //displayAddPopup = true;
            }
            //Medicine_List__c medicinelist = Medicine_List__c();
            //medicinelist.Name = medicine.Id;
            //medicinelist.Strength__c='test';
            //medicinelist.Formulation__c = 'test';  
             //try { 
              //  insert medicinelist;
            //} catch(DmlException e) {
             //   ApexPages.addMessages(e); 
             //   errorMessage = 'Duplicate found in Medicine List';
              //  displayAddPopup = true;
            //}
              displayAddPopupMedicine = false;
        }
       /* Patient_Diagnosis__c patientDiagnos = new Patient_Diagnosis__c();
        patientDiagnos.Diagnosis__c = diagnos.Id;
        patientDiagnos.Patient__c = patientId;
        patientDiagnos.Active__c = true;
            try { 
                insert patientDiagnos;
            } catch(DmlException e) {
                ApexPages.addMessages(e); 
                errorMessage = 'Duplicate found in Patient Diagnosis';
              //  displayAddPopup = true;
            }
        displayAddPopup = false;
        }*/
        system.debug('one - 70 '+ displayAddPopupMedicine);
    }
     
  //  Close Medicine
  
    public void saveSOAPNote() {
        update SOAPNoteObj;
        closePopup();
    }
    
    /*public PatientSummary(ApexPages.StandardController stdController) {
        this.con = (Contact)stdController.getRecord();
        patientInfo = [Select Name, Gender__c, Age__c, Email, Birthdate from Contact where Id=: patientId ];
        patientName = patientInfo[0].Name;
        patientGender = patientInfo[0].Gender__c;
        patientDOB = patientInfo[0].Birthdate;
        patientAge = patientInfo[0].Age__c;
        email = patientInfo[0].Email;
        system.debug('Patient Id - '+patientId);
        List<Visit__c> visit1 = new List<Visit__c>([Select Id, Patient__c from Visit__c]);
        system.debug('Visit Id - '+visit1);
        if(patientId != null) {
            List<Visit__c> visit = new List<Visit__c>([Select Id from Visit__c where Patient__c =: patientId Order By CreatedDate desc Limit 1 ]);
            system.debug('Visit Id - '+visit[0].Id);
            visitId = visit[0].Id;
        }
        currentuser=new User();
     	currentuser=[Select Id,Name,Email from User where Id=:userinfo.getuserId()];
        system.debug('--mlWrapper--'+mlWrapper+' ---selectedPatientMedicineList---'+selectedPatientMedicineList);
    }*/
    
    public List<Patient_Allergy__c> getAllergies() {
        if(allergies == null) 
            allergies = [select Id, Allergy__r.Name from Patient_Allergy__c where Patient__c =: patientId];
        return allergies;
    }
    
    public List<SOAP_Note__c> getSOAPNotes() {
        if(patientSOAPNotes == null) 
            patientSOAPNotes = [select Id, Name, CreatedDate, Soap_Assessment__c from SOAP_Note__c where Visit__r.Patient__c =: patientId Order By CreatedDate Desc];
        return patientSOAPNotes;
    }  
    
    public List<Patient_Diagnosis__c> getDiagnosis() {
        if(patientDiagnosis == null) 
            patientDiagnosis = [select Id, Diagnosis__r.Name, Diagnosis__r.Description__c from Patient_Diagnosis__c where Patient__c =: patientId and Active__c = false];
        return patientDiagnosis;
    }
    
    public List<Patient_Diagnosis__c> getProblemList() {
        if(patientProblemList == null) 
            patientProblemList = [select Id, Diagnosis__r.Name, Diagnosis__r.Description__c,Patient__c from Patient_Diagnosis__c where Patient__c =: patientId and Active__c = true];
        return patientProblemList;
    }
    
    public List<MedicineListWrapper> getMedication() {
        //if(patientMedication == null) 
            patientMedication = [select Id, Medicine_List__r.Name, Medicine_List__r.Strength__c, Medicine_List__r.Formulation__c, Dose__c, Route_of_Administration__c, Injection_doses__c, Duration__c, Refill__c, Frequency__c from Patient_Medicine__c where Patient__c =: patientId];
        mlWrapper = new List<MedicineListWrapper>();
        
        for (Patient_Medicine__c pm : patientMedication) {
            mlWrapper.add(new MedicineListWrapper(pm));
        }
        return mlWrapper;
    }
    
    public List<Patient_Medicine__c> getselectedPatientMedicineList1() {
        return selectedPatientMedicineList;
    }
    
    public List<Lab_Report__c> getlabReport() {
        if(patientlabData == null) 
            patientlabData = [select Id, CreatedDate, RecordType.Name from Lab_Report__c where Patient__c =: patientId];
        return patientlabData;
    }
    
    public List<Vitals__c> getVitalRecord() {
        if(patientVital == null) 
            patientVital = [select Id, Name, BP_Diastolic__c, BP_Systolic__c, Case__c, Heart_Rate_bits_min__c,BMI__c,
                            calculateBMI__c, Height_cms__c, Patient__c, Patient_Full_Name__c, Temp_F__c, Weight_kgs__c, 
                            CreatedDate from Vitals__c where Patient__c =: patientId order by createddate desc];
        return patientVital;
    }
    
    public SOAP_Note__c getSOAPNote() {
        if(SOAPNote == null) SOAPNote = new SOAP_Note__c();
        return SOAPNote;
    }
    
    public PageReference saveSOAPNotes() {
        if(visitId != null) {
            SOAPNote.Visit__c = visitId;
            errorMessage = null;
            try {
                insert SOAPNote;
                SOAPNote = new SOAP_Note__c();
            } catch(DmlException e) {
                ApexPages.addMessages(e);
                system.debug('==Exception=='+e);
                errorMessage = 'Please provide all the details for the SOAP Notes';
            }
        }
        return null;
    }
    
    public void fetchSOAPRecord(){  
        if(selectedSOAPId != null){  
            List<SOAP_Note__c> soapnotebyId = [  
                SELECT Id, Name, Soap_Assessment__c, Soap_Objective__c, Soap_Plan__c, Subjective__c, Visit__r.Name  
                FROM SOAP_Note__c  
                WHERE Id =: selectedSOAPId  
                ORDER BY Name ASC  
            ];  
            jsonDataSOAPRecord = JSON.serialize(soapnotebyId);
        }  
    }
    
    public PageReference createPrescription() {
        System.debug('--mlWrapper--'+mlWrapper);
        //selectedPatientMedicineList = new List<Patient_Medicine__c>();
        for(MedicineListWrapper mlwrapp: mlWrapper){
            if(mlwrapp.selected == true){
                selectedPatientMedicineList.add(mlwrapp.obj);
            }
        }
        system.debug('--selectedPatientMedicineList--'+selectedPatientMedicineList);
        if(selectedPatientMedicineList.size()<1){
            system.debug('==Exception Occured==');
            //ApexPages.addMessages(new DmlException());
            //system.debug('==Exception=='+e);
            apexpages.addMessage(new ApexPages.message(Apexpages.Severity.ERROR,'Please select atlest one patient medicine'));
            //errorMessage = 'Please select atlest one patient medicine';
        }else{
            system.debug('Redirect to PrescriptionDetails page'+selectedPatientMedicineList);
            //PageReference pr = new PageReference('/apex/PrescriptionDetails');
            //pr.getParameters().put('id',patientId);
            //pr.setRedirect(false);
            return page.PrescriptionDetails;
        }
        return null;
    }
    
    public PageReference previewasPDF(){
		return page.previewPrescription;
        
    }
    
    public PageReference SendEmail(){
        return page.EmailAttachment;
    }
    
    public PageReference cancel(){
        return page.PatientSummaryPage;
        //PageReference pr = new PageReference('/apex/PatientSummaryPage');
        //pr.getParameters().put('id',patientId);
        //pr.setRedirect(false);
        //return pr;
    }
    
    public class MedicineListWrapper {
        public Patient_Medicine__c obj { get; set; }
        public Boolean selected { get; set; }
        
        public MedicineListWrapper(Patient_Medicine__c pm) {
            this.obj = pm;
            this.selected = false;
        }
    }
    
    /*@RemoteAction
    public static string SendAttachment(String sEmailAddress, String patientId){
        String sMessage='';
        try{            
            Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            PageReference pref = page.previewPrescription;
            pref.getParameters().put('id',patientId);
            pref.setRedirect(true);
            Blob b = pref.getContent();
            attach.setFileName('Precription.pdf');
            attach.setBody(b);
            semail.setSubject('Precription');
            semail.setToAddresses(new List<String>{sEmailAddress});
            semail.setPlainTextBody('Please find the attached Precription');
            semail.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{semail});
            sMessage='SUCCESS';
            
            //-----------------attachg as the pdf--------------------
            Attachment myAttach = new Attachment();
            myAttach.ParentId = patientId;//Id of the object to which the page is attached
            myAttach.name = 'Precription.pdf';
            PageReference myPdf = ApexPages.currentPage();//myPdfPage is the name of your pdf page
            myAttach.body = b;
            insert myAttach;
        }
        catch(Exception ex){
            sMessage=ex.getMessage()+'\n'+ex.getLineNumber()+'\n'+ex.getCause();
        }
        return sMessage;
    }*/
    
    public PageReference SendAttachment1(){
        String sMessage='';
        try{            
            Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            PageReference pref = page.previewPrescription;
            pref.getParameters().put('id',patientId);
            pref.setRedirect(true);
            Blob b = pref.getContent();
            attach.setFileName('Prescription.pdf');
            attach.setBody(b);
            semail.setSubject('Prescription');
            semail.setToAddresses(new List<String>{email});
            semail.setPlainTextBody('Please find the attached Prescription');
            semail.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});
            List<OrgWideEmailAddress> lstEmailAddress=[select Id from OrgWideEmailAddress WHERE Address='info@cloudmdfoundation.org'];       
        	// Set Organization-Wide Email Address Id
        	semail.setOrgWideEmailAddressId(lstEmailAddress[0].Id);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{semail});  
            sMessage='SUCCESS';
            
            //-----------------attachg as the pdf--------------------
            Attachment myAttach = new Attachment();
            myAttach.ParentId = patientId;//Id of the object to which the page is attached
            myAttach.name = 'Prescription.pdf';
            PageReference myPdf = ApexPages.currentPage();//myPdfPage is the name of your pdf page
            myAttach.body = b;
            insert myAttach;
            PageReference pr = new PageReference('/servlet/servlet.FileDownload?file=' + myAttach.Id);
            //pr.getParameters().put('id',patientId);
            //pr.setRedirect(false);
            return pr;
        }
        catch(Exception ex){
            sMessage=ex.getMessage()+'\n'+ex.getLineNumber()+'\n'+ex.getCause();
            return null;
        }
        return null;
    }
}