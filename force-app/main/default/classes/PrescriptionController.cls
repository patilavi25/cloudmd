public class PrescriptionController {
    public List<Patient_Medicine__c> SelectedpatientMedication;
    public user currentuser{get;set;}
    public String  Licensenumber {get;set;}
    public PageReference openPrint() {
        //PageReference page = new PageReference('apex/PatientDetails?Id='+visitId);
        return page.PatientDetails;
    }

    public List<Contact> LstContact {get; set;}
    public DateTime DateOfVisit {get; set;}
    public String ReasonOfVisit {get; set;}
    public List<Visit__c> LstVisit {get; set;}
    public Boolean ShowAllergy {get; set;}
    public Boolean ShowMedicine {get; set;}
    //public Date todayDate {get; set;}
    String visitId;
    
    public PrescriptionController()
    {
        currentuser=new User();
     	currentuser=[Select Id,Name,Email from User where Id=:userinfo.getuserId()];
        visitId = ApexPages.currentPage().getParameters().get('Id');
        visitId = 'a0D2g000000cgnr';
        LstVisit = [Select Patient__c, Check_In_time__c, ReasonforVisit__c, Onsite_Physician__r.Name, 
                        (Select Soap_Assessment__c, Soap_Objective__c, Soap_Plan__c, Subjective__c From SOAP_Notes__r) 
                        From Visit__c Where Id =:visitId];
        //DateOfVisit = lstVisit[0].Check_In_time__c;
        //ReasonOfVisit = lstVisit[0].ReasonforVisit__c;
        
        LstContact = [Select Id, Name, Patient_ID__c, DOB__c, Account.Name, Birthdate,
                            (Select Height_cms__c, Weight_kgs__c, Heart_Rate_bits_min__c, BP_Diastolic__c, BP_Systolic__c, Temp_F__c, BMI__c From Vitals__r), 
                            (Select Allergy__r.Name, Allergy_Type__c From Patient_Allergies__r), 
                            (Select Medicine_List__r.Name, Medicine_List__r.Strength__c, Medicine_List__r.Formulation__c, Dose__c, 
                                Route_of_Administration__c, Frequency__c, Duration__c, Dispnese__c, Refill__c From Patient_Medicines__r) 
                            From Contact Where Id =:lstVisit[0].Patient__c];
    }
    
    public void createPrescription(){
        SelectedpatientMedication = [Select Name, Medicine_List__r.Strength__c, Medicine_List__r.Formulation__c, Dose__c, 
                                	Route_of_Administration__c, Frequency__c, Duration__c, Dispnese__c, Refill__c From Patient_Medicine__c];
        SelectedpatientMedication = SelectedpatientMedication;
    }
}