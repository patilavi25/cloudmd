@IsTest
public class Test_PatientCheckInController {
    public static testmethod void PatientCheckInControllerTest_CheckIn() {
        PatientCheckInController PCIController = new PatientCheckInController();
        PCIController.OpenNewPatient();
        PCIController.visitRecord  = new Visit__c();
        Contact c = new Contact();
        c.LastName = 'ln';
        insert c ;
        
        PCIController.visitRecord.Patient__c = c.Id;
        PCIController.visitRecord.Check_In_time__c = Date.Today();
        PCIController.visitRecord.ReasonforVisit__c = 'reason for visit';
        PCIController.saveVisit();
    }
}