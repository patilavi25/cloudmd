public class NewVitalsExtension {
    public Contact contact {get;set;}
    public Vitals__c myVitals{get;set;}
    public NewVitalsExtension(ApexPages.StandardController stdController) {
        String val = apexpages.currentpage().getparameters().get('edit');
        if(myVitals == null && val != 'true'){  
            myVitals = new Vitals__c();
            myVitals.Height_cms__c = null;
            myVitals.Weight_kgs__c = null;
            myVitals.Weight_Unit__c = null;
            myVitals.Height_Unit__c = 'cm';
            myVitals.Heart_Rate_bits_min__c = null;
            myVitals.BP_Systolic__c = null;
            myVitals.BP_Diastolic__c = null;
            myVitals.Temp_F__c = null;
            myVitals.Visit__c = apexpages.currentpage().getparameters().get('visitId');
            myVitals.Patient__c = apexpages.currentpage().getparameters().get('contactId');
            contact = new Contact();
            Id contactId = apexpages.currentpage().getparameters().get('contactId');
            system.debug('contactId '+apexpages.currentpage().getparameters().get('edit'));
        }else{
            string vitalIdFromUrl = apexpages.currentpage().getparameters().get('Id');
            myVitals = [SELECT Height_cms__c, Weight_kgs__c,Height_Unit__c, 
                        Weight_Unit__c, Heart_Rate_bits_min__c, 
                        BP_Systolic__c, BP_Diastolic__c, Temp_F__c, 
                        Visit__c, Patient__c, calculateBMI__c
                        FROM Vitals__c WHERE Id = :vitalIdFromUrl]; //query for the vitalsId;
            if(myVitals.Height_Unit__c == null){
                myVitals.Height_Unit__c = 'cm';
            }
            system.debug('newvitals edit in else '+myVitals );
        }
        
    }
    
    public PageReference  save(){
        //Vitals__c newVitals = new Vitals__c(myVitals);
        Decimal heightincm = myVitals.Height_cms__c;
        Decimal weightinKg = myVitals.Weight_kgs__c;
        if(myVitals.Height_Unit__c == 'in'){
            heightincm =  myVitals.Height_cms__c * 2.54;
        }if(myVitals.Weight_Unit__c == 'lbs'){
            weightinKg =  myVitals.Weight_kgs__c / 2.205;
        }
        Decimal BMI = weightinKg / (heightincm*heightincm*0.0001);
        myVitals.calculateBMI__c = Math.round(BMI * 100) / 100.0;
        upsert myVitals;
        PageReference orderPage = new PageReference('/' + myVitals.id);
        orderPage.setRedirect(true);
        return orderPage;
        //system.debug('yekarkdikhao '+ myVitals.Weight_kgs__c + ' >>>>>> ' + myVitals);
    }
}