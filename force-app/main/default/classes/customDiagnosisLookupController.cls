public class customDiagnosisLookupController {
  
  public Diagnosis__c diagnosis {get;set;} // new account to create
  public List<Diagnosis__c> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  
  public customDiagnosisLookupController() {
    diagnosis = new Diagnosis__c();
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();  
  }
   
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
  
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 
  
  // run the search and return the records found. 
  private List<Diagnosis__c> performSearch(string searchString) {

    String soql = 'select id, name, Description__c from Diagnosis__c';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where Name LIKE \'%' + searchString +'%\'';
      soql = soql + ' limit 100';
    System.debug(soql);
    return database.query(soql); 

  }
  
  // save the new diagnosis record
  public PageReference saveDiagnosis() {
      try{
          insert diagnosis;
          results.add(diagnosis);
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'record created successfully, please click search'));
      }catch(DmlException e){
          System.debug('The following exception has occurred: ' + e.getMessage());
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
      } 
    // reset the diagnosis
    diagnosis = new Diagnosis__c();
    return null;
  }
  
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
}