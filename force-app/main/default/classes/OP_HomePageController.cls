public without sharing class OP_HomePageController {
    public List<Visit__c> lstVisit{get;set;}
    public List<Lab_Report__c> lstLabReport{get;set;}
    public List<Task> lstTask{get;set;}
    public List<Slot__c> lstTodaySlots  {get;set;}
    public List<Slot__c> lsttomorrowOnwardSlots  {get;set;}
    public List<Consulting__c> todaysConsulting {get;set;}
    public List<Consulting__c> cancelledConsulting {get;set;}
    
    public Date fromDate {get;set;}
    //public Date toDate {get;set;}
    public List<Visit__c> visitLst{get;set;}
    
    public OP_HomePageController (){
        fromDate = Date.today().addDays(-7);
        //toDate = Date.today();
        getMyPatientListOnSelecteddate();
        getMyPatientQueue();
        getMyUnviewedReports();
        getInbox();
        getScheduledConsultations();
        getScheduledCancelledConsultations();
    }
    
    public void getMyPatientQueue(){
        lstVisit = new List<Visit__c>();
        List<Visit__c> lstlclVisit = [Select Id, Name,Patient__r.Name, Patient__r.Patient_ID__c, Check_In_time__c, 
                                      Patient__r.Account.Name, Onsite_Physician__r.Name, Onsite_Physician__c, 
                                      (Select Id from SOAP_Notes__r Limit 1) 
                                      from Visit__c Where Onsite_Physician__c =:userinfo.getuserId() 
                                      and DAY_ONLY(convertTimezone(Check_In_time__c)) >=: Date.today() Order By Check_In_time__c ASC ]; 
        system.debug('--lstlclVisit--'+lstlclVisit);
        for( Visit__c v : lstlclVisit){
            if(v.SOAP_Notes__r.size() == 0 ) {
                lstVisit.add(v);
            }
        }
    }
    
    public void getMyPatientListOnSelecteddate(){
        visitLst = new List<Visit__c>();
        for( Visit__c v : [Select Id, Name,Patient__r.Name, Patient__r.Patient_ID__c, Check_In_time__c, 
                                      Patient__r.Account.Name, Onsite_Physician__r.Name, Onsite_Physician__c, 
                                      (Select Id from SOAP_Notes__r Limit 1) 
                                      from Visit__c Where Onsite_Physician__c =:userinfo.getuserId() 
                                      and DAY_ONLY(convertTimezone(Check_In_time__c)) =: date.today() Order By Check_In_time__c Desc]){
            if(v.SOAP_Notes__r.size() > 0 ) {
                visitLst.add(v);
            }
        }
        for(Visit__c v : [Select Id, Name,Patient__r.Name, Patient__r.Patient_ID__c, Check_In_time__c, 
                                      Patient__r.Account.Name, Onsite_Physician__r.Name, Onsite_Physician__c, 
                                      (Select Id from SOAP_Notes__r Limit 1) 
                                      from Visit__c Where Onsite_Physician__c =:userinfo.getuserId() 
                                      and DAY_ONLY(convertTimezone(Check_In_time__c)) >=: fromDate and DAY_ONLY(convertTimezone(Check_In_time__c)) <: date.today() Order By Check_In_time__c Desc]){
                                         visitLst.add(v); 
                                      }    
    }
    
    public void getMyUnviewedReports() {
        lstLabReport = new List<Lab_Report__c>();
        lstLabReport = [Select RecordType.Name, Patient__r.Patient_ID__c,CreatedDate, Patient__r.Name, Name from Lab_report__c 
                        where reviewed__c =: false];
    }
    
    public void  getInbox() {
        lstTask = [Select t.Who.Name, t.WhatId, t.Subject, t.Status, t.Priority, t.OwnerId, t.IsRecurrence, t.IsClosed,
                   t.owner.Name, t.ActivityDate,
                   CreatedBy.Name From Task t
                   where t.OwnerId =: userinfo.getuserId() and isClosed=false ];
    }
    
    public void  getScheduledConsultations() {
        Date tToday = Date.Today();
        Date SeventhDayfromToday = Date.Today()+7;
        DateTime currentTime = DateTime.now();
        todaysConsulting = [Select Id, Name,visit__r.Patient__r.Name, Slot__r.Start_Time__c, HangoutURL__c, Slot__r.End_Time__c, CreatedBy.Name, visit__r.Name,User__r.Name,
                            createdby.BlueJeansMeetingPersonalId__c from Consulting__c where User__c =: userinfo.getuserId() 
                            and visit__r.Patient__c != null and  Slot__r.Start_Time__c != null and isConfirmed__c = true
                            and DAY_ONLY(convertTimezone(Slot__r.Start_Time__c))  >=: tToday  
                            and Slot__r.Start_Time__c >=: currentTime and slot__r.cancelled__c = false Order by Slot__r.Start_Time__c  ];
    }
    
    public void  getScheduledCancelledConsultations() {
        Date tToday = Date.Today();
        Date SeventhDayfromToday = Date.Today()+7;
        DateTime currentTime = DateTime.now();
        List<Consulting__c >   cancelledlclConsulting   = new  List<Consulting__c > ();
        cancelledConsulting = new List<Consulting__c > ();
        cancelledlclConsulting   = [Select Name,visit__r.Patient__r.Name, Slot__r.Start_Time__c, Slot__r.End_Time__c, CreatedBy.Name, visit__r.Name,User__r.Name,
                                    createdby.BlueJeansMeetingPersonalId__c, (Select Id from New_Consulting__r Limit 1) from Consulting__c where createdbyId =: userinfo.getuserId() 
                                    and visit__r.Patient__c != null and  Slot__r.Start_Time__c != null 
                                    and DAY_ONLY(convertTimezone(Slot__r.Start_Time__c))  >=: tToday and DAY_ONLY(convertTimezone(Slot__r.Start_Time__c)) <=:SeventhDayfromToday 
                                    and Slot__r.Start_Time__c >=: currentTime and 
                                    slot__r.cancelled__c = true Order by Slot__r.Start_Time__c  ];
        
        for(Integer i = 0 ; i < cancelledlclConsulting.size() ; i++ ) {
            if(cancelledlclConsulting[i].New_Consulting__r != null && cancelledlclConsulting[i].New_Consulting__r.size()  == 0){
                cancelledConsulting.add(cancelledlclConsulting[i]);    
            }
        }
    }
}