@IsTest
public class Test_LabReportControls {
    public static testmethod void LabReportControls_Test() {
        Contact c = new Contact();
        c.LastName = 'Patient';
        insert c ;
        
        Lab_Report__c lr = new Lab_report__c(Lab_Date__c = system.today(), Patient__c = c.Id);
        insert lr;
        //Call the Method
        test.startTest();
        LabReportControls.markReviewedLabReport(lr.id);
        test.stopTest();
    }
}