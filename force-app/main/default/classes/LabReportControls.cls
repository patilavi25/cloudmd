global class LabReportControls {
    webservice static void markReviewedLabReport(Id reportId ) { 
        Lab_Report__c lr = new Lab_report__c(id = ReportId );
        lr.reviewed__c = true ;
        update lr;      
    }
}