@IsTest
public class Test_VitalsEditRedirect {
    public static testmethod void Redirect(){
        Vitals__c v = new Vitals__c();
         Contact p = new Contact();
        p.LastName = 'Patient';
        p.Gender__c = 'Male';
        p.Birthdate = system.today();
        insert p ;
            v.Patient__c = p.id;
        insert v;
         //Test the Controller.
        ApexPages.StandardController beforeController = new ApexPages.StandardController(v);
        VitalsEditRedirect beforeExtension = new VitalsEditRedirect(beforeController);
        beforeExtension.redirectToAnotherVisualforcePage();
    }
}