public class Zoom_MeetingObject {

    public class Global_dial_in_numbers {
        public String city {get;set;} 
        public String country {get;set;} 
        public String country_name {get;set;} 
        public String number_Z {get;set;} // in json: number
        public String type_Z {get;set;} // in json: type

        public Global_dial_in_numbers(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'city') {
                            city = parser.getText();
                        } else if (text == 'country') {
                            country = parser.getText();
                        } else if (text == 'country_name') {
                            country_name = parser.getText();
                        } else if (text == 'number') {
                            number_Z = parser.getText();
                        } else if (text == 'type') {
                            type_Z = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Global_dial_in_numbers consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public String created_at {get;set;} 
    public Integer duration {get;set;} 
    public String encrypted_password {get;set;} 
    public String h323_password {get;set;} 
    public String host_id {get;set;} 
    public Long id {get;set;} 
    public String join_url {get;set;} 
    public String password {get;set;} 
    public String pstn_password {get;set;} 
    public Settings settings {get;set;} 
    public String start_time {get;set;} 
    public String start_url {get;set;} 
    public String status {get;set;} 
    public String timezone {get;set;} 
    public String topic {get;set;} 
    public Integer type_Z {get;set;} // in json: type
    public String uuid {get;set;} 
    public String schedule_for {get;set;} 
    public Integer total_minutes {get;set;}
    public Integer participants_count {get;set;}
    public String user_name {get;set;}
    public String user_email {get;set;}
    public String end_time {get;set;} 

    public Zoom_MeetingObject(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'created_at') {
                        created_at = parser.getText();
                    } else if (text == 'duration') {
                        duration = parser.getIntegerValue();
                    } else if (text == 'encrypted_password') {
                        encrypted_password = parser.getText();
                    } else if (text == 'h323_password') {
                        h323_password = parser.getText();
                    } else if (text == 'host_id') {
                        host_id = parser.getText();
                    } else if (text == 'id') {
                        id = parser.getLongValue();
                    } else if (text == 'join_url') {
                        join_url = parser.getText();
                    } else if (text == 'password') {
                        password = parser.getText();
                    } else if (text == 'pstn_password') {
                        pstn_password = parser.getText();
                    } else if (text == 'settings') {
                        settings = new Settings(parser);
                    } else if (text == 'start_time') {
                        start_time = parser.getText();
                    } else if (text == 'start_url') {
                        start_url = parser.getText();
                    } else if (text == 'status') {
                        status = parser.getText();
                    } else if (text == 'timezone') {
                        timezone = parser.getText();
                    } else if (text == 'topic') {
                        topic = parser.getText();
                    } else if (text == 'type') {
                        type_Z = parser.getIntegerValue();
                    } else if (text == 'schedule_for') {
                        schedule_for = parser.getText();
                    } else if (text == 'uuid') {
                        uuid = parser.getText();
                    } else if(text == 'total_minutes'){
                        total_minutes = parser.getIntegerValue();
                    } else if(text == 'user_name'){
                        user_name = parser.getText();
                    } else if(text == 'user_email'){
                        user_email = parser.getText();
                    } else if(text == 'end_time'){
                        end_time = parser.getText();
                    } else if(text == 'participants_count'){
                        participants_count = parser.getIntegerValue();
                    } else {
                        System.debug(LoggingLevel.WARN, 'ZoomJsonObject consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class Settings {
        public String alternative_hosts {get;set;} 
        public Integer approval_type {get;set;} 
        public String audio {get;set;} 
        public String auto_recording {get;set;} 
        public Boolean close_registration {get;set;} 
        public Boolean cn_meeting {get;set;} 
        public Boolean enforce_login {get;set;} 
        public String enforce_login_domains {get;set;} 
        public List<String> global_dial_in_countries {get;set;} 
        public List<Global_dial_in_numbers> global_dial_in_numbers {get;set;} 
        public Boolean host_video {get;set;} 
        public Boolean in_meeting {get;set;} 
        public Boolean join_before_host {get;set;} 
        public Boolean meeting_authentication {get;set;} 
        public Boolean mute_upon_entry {get;set;} 
        public Boolean participant_video {get;set;} 
        public Boolean registrants_confirmation_email {get;set;} 
        public Boolean use_pmi {get;set;} 
        public Boolean waiting_room {get;set;} 
        public Boolean watermark {get;set;} 
        public Boolean registrants_email_notification {get;set;} 

        public Settings(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'alternative_hosts') {
                            alternative_hosts = parser.getText();
                        } else if (text == 'approval_type') {
                            approval_type = parser.getIntegerValue();
                        } else if (text == 'audio') {
                            audio = parser.getText();
                        } else if (text == 'auto_recording') {
                            auto_recording = parser.getText();
                        } else if (text == 'close_registration') {
                            close_registration = parser.getBooleanValue();
                        } else if (text == 'cn_meeting') {
                            cn_meeting = parser.getBooleanValue();
                        } else if (text == 'enforce_login') {
                            enforce_login = parser.getBooleanValue();
                        } else if (text == 'enforce_login_domains') {
                            enforce_login_domains = parser.getText();
                        } else if (text == 'global_dial_in_countries') {
                            global_dial_in_countries = arrayOfString(parser);
                        } else if (text == 'global_dial_in_numbers') {
                            global_dial_in_numbers = arrayOfGlobal_dial_in_numbers(parser);
                        } else if (text == 'host_video') {
                            host_video = parser.getBooleanValue();
                        } else if (text == 'in_meeting') {
                            in_meeting = parser.getBooleanValue();
                        } else if (text == 'join_before_host') {
                            join_before_host = parser.getBooleanValue();
                        } else if (text == 'meeting_authentication') {
                            meeting_authentication = parser.getBooleanValue();
                        } else if (text == 'mute_upon_entry') {
                            mute_upon_entry = parser.getBooleanValue();
                        } else if (text == 'participant_video') {
                            participant_video = parser.getBooleanValue();
                        } else if (text == 'registrants_confirmation_email') {
                            registrants_confirmation_email = parser.getBooleanValue();
                        } else if (text == 'use_pmi') {
                            use_pmi = parser.getBooleanValue();
                        } else if (text == 'waiting_room') {
                            waiting_room = parser.getBooleanValue();
                        } else if (text == 'watermark') {
                            watermark = parser.getBooleanValue();
                        } else if (text == 'registrants_email_notification') {
                            registrants_email_notification = parser.getBooleanValue();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Settings consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static Zoom_MeetingObject parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new Zoom_MeetingObject(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    




    private static List<String> arrayOfString(System.JSONParser p) {
        List<String> res = new List<String>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(p.getText());
        }
        return res;
    }


    private static List<Global_dial_in_numbers> arrayOfGlobal_dial_in_numbers(System.JSONParser p) {
        List<Global_dial_in_numbers> res = new List<Global_dial_in_numbers>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Global_dial_in_numbers(p));
        }
        return res;
    }




}