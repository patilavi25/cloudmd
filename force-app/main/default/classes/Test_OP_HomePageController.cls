@IsTest
public class Test_OP_HomePageController {
    public static testmethod void OP_Home_Test() {
        //Create a Patient
        Contact p = new Contact();
        p.LastName = 'Patient';
        insert p ;
        
        //Create a Visit
        Visit__c v = new Visit__c (Check_In_time__c = system.now(), Onsite_Physician__c = UserInfo.getUserId(), Patient__c = p.Id, 
                                   ReasonforVisit__c = 'Sick');
        
        //Create Availabilities for Remote Physician
        Availability__c av = new Availability__c(User__c = UserInfo.getUserId(), Start_Date__c = system.today(), End_Date__c = system.today()+1, 
                                                 From_Time__c = '00:15 AM', To_Time__c = '11:30 PM', Sunday__c = True, Monday__c = True,
                                                 Tuesday__c = True, Wednesday__c = True, Thrusday__c = True, Friday__c = True, Saturday__c = True);
        insert av;
        
        //Query Slots
        List<Slot__c> slot = new List<Slot__c>([Select Id, Availability__c, Physician__c, Start_Time__c, End_Time__c, Is_Booked__c, Is_Future_Slot__c, Cancelled__c  from Slot__c order by End_Time__c Desc limit 1]);
        system.debug('Slots - '+slot);
        //Create Consulting Record
        Consulting__c con = new Consulting__c(User__c = UserInfo.getUserId(), Reason__c = 'Sick', Slot__c = slot[0].Id );
        insert con;
        
        //Test the Controller.
        OP_HomePageController controller = new OP_HomePageController();
        controller.getInbox();
        controller.getMyPatientQueue();
        controller.getScheduledCancelledConsultations();
        controller.getScheduledConsultations();
    }
}