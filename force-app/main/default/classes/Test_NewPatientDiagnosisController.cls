@IsTest
public class Test_NewPatientDiagnosisController {
    public static testmethod void NewPatientDiagnosis() {
        
        // Put Id into the current page Parameters
        
        Contact patient = new Contact();
        patient.LastName = 'Patient';
        patient.Gender__c = 'Male';
        patient.Birthdate = system.today();
        insert patient ;
        
        Diagnosis__c d = new Diagnosis__c();
        d.Name = 'test';
        d.Description__c='testD';
        insert d;
        
        Patient_Diagnosis__c pd = new Patient_Diagnosis__c();
        pd.Diagnosis__c = d.Id;
        pd.Patient__c=patient.Id;
        
        ApexPages.StandardController beforeController = new ApexPages.StandardController(pd);
        NewPatientDiagnosisController beforeExtension = new NewPatientDiagnosisController(beforeController);
        
        ApexPages.StandardController controller = new ApexPages.StandardController(pd);
        ApexPages.currentPage().getParameters().put('contactId', patient.Id);
        
        Test.startTest();
        NewPatientDiagnosisController extension = new NewPatientDiagnosisController(controller);
        extension.pd = pd;
        extension.save();
        extension.Cancel();
        Test.stopTest();
    }
}