public with sharing class PatientCheckInController {
    public Visit__c visitRecord{get;set;}
    public PageReference saveVisit() {
        insert visitRecord;
        return null;
    }
    
    public PageReference OpenNewPatient() {
        PageReference p = new PageReference('/003/e?retURL=%2Fapex%2FPatientCheckIn');
        return p;
    }
    
    public PatientCheckInController() {
        visitRecord = new Visit__c();
        visitRecord.Check_In_time__c = system.now();
    }
}