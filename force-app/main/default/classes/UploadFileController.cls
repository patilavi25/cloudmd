public without sharing class UploadFileController {
    
    public String recordId {get;set;}
    public Blob AttchBody {get;set;}
    public String AttchDesc {get;set;}
    public String AttchNamecustom {get;set;}
    public String AttchName {get;set;}
    Public Attachment attch {get;set;}
    Public Attachment accSign {get;set;}
    public Integer filesize {get;set;}
    public String docId {get;set;}
    public List<Document> documentList;
    List<Physician_Info__c> physicianInfo;
    public boolean WatermarkAlreadyPresent {get;set;}
    public String imageURL {get;set;}
    public boolean signatureAlreadyPresent {get;set;}
    public String signatureURL {get;set;}
    
    public UploadFileController(){
        //AttchName = 'nonetest';
        documentList = [SELECT Id, Name, DeveloperName, Body, ContentType, Type, IsPublic, AuthorId, FolderId from Document WHERE Name= 'Watermark Image'];
        if(documentList.size()>0){
            WatermarkAlreadyPresent = true;
            imageURL = '/servlet/servlet.FileDownload?file=' + documentList[0].Id;
        }
        
        physicianInfo = [Select id, Name, Physician__c, SignatureURL__c, WatermarkImageURL__c, OwnerId From Physician_Info__c Where Physician__c=:userinfo.getuserId() limit 1];
        if(physicianInfo.size()>0){
            if(physicianInfo[0].SignatureURL__c!=null){
                signatureAlreadyPresent = true;
                signatureURL = physicianInfo[0].SignatureURL__c.substringAfter('.com');
            }
        }
    }
    
    public PageReference UploadWatermark(){
        system.debug('AttchName: '+AttchName+' AttchDesc: '+'AttchNamecustom: '+AttchNamecustom+' File size: '+filesize);
        
        if(AttchName==null || AttchName==''){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select the file'));
            return null;
        }
        
        if(!AttchName.containsIgnoreCase('.jpg') && !AttchName.containsIgnoreCase('.jpeg') && !AttchName.containsIgnoreCase('.png')){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select image file'));
            return null;
        }
        
        try {
            User currentuser=new User();
            currentuser=[Select Id,Name,Email,Signature__c from User where Id=:userinfo.getuserId()];
                       
            if(physicianInfo.size() < 1){
                Physician_Info__c physician =new Physician_Info__c();
                physician.Name = currentuser.Name;
                physician.Physician__c = currentuser.Id;
                insert physician;
                physicianInfo.add(physician);
            }
            
            String fileName = 'Watermark '+currentuser.Name; 
            List<Attachment> attachmentList = [SELECT Id from Attachment WHERE ParentId=:physicianInfo[0].Id AND Name=:fileName];
            accSign = new Attachment();
            accSign.ParentID = physicianInfo[0].Id;
            //accSign.ParentID = '005j000000BkJiU';	
            accSign.Body = AttchBody;
            accSign.contentType = 'image/png';
            accSign.Name = 'Watermark '+currentuser.Name;
            accSign.OwnerId = UserInfo.getUserId();
            accSign.Description=AttchDesc;
            //insert accSign;
            
            if(attachmentList.size() > 0){
                //delete attachmentList; 
            }
            
            //String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm();
            //String signatureURL = sfdcURL+'/servlet/servlet.FileDownload?file=' + accSign.Id;
            
            //update signature URL on user recod
            //physicianInfo[0].WatermarkImageURL__c = signatureURL;
            //update physicianInfo;
            
            //String docId;
            //List<Document> documentList = [SELECT Id, Name, DeveloperName, Body, ContentType, Type, IsPublic, AuthorId, FolderId from Document WHERE Name= 'Watermark Image'];
            if(documentList.size()>0){
                documentList[0].Body = AttchBody;
                documentList[0].ContentType = 'image/png';
                documentList[0].Type = 'png';
                documentList[0].IsPublic = true;
                update documentList;
                docId = documentList[0].Id;
            }else{
                Document doc = new Document();
                doc.Name = 'Watermark Image';
                //doc.DeveloperName = 'Watermark_Image';
                doc.Body = AttchBody;
                doc.ContentType = 'image/png';
                doc.Type = 'png';
                doc.IsPublic = true;
                doc.AuthorId = UserInfo.getUserId();
                doc.FolderId = '00lj0000000ztvj';
                insert doc;
                docId = doc.Id;
            }
    
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getlineNumber()+' '+ex.getMessage()));
            return null;
            
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Watermark image uploaded successfully'));
        PageReference showImage = new PageReference('/servlet/servlet.FileDownload?file=' + docId);
        showImage.setRedirect(true);
        return showImage;
    }
    
    public PageReference UploadSignature(){

        system.debug('AttchName: '+AttchName+' AttchDesc: '+'AttchNamecustom: '+AttchNamecustom+' File size: '+filesize);        
        if(AttchName==null || AttchName==''){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select the file'));
            return null;
        }
        
        if(!AttchName.containsIgnoreCase('.jpg') && !AttchName.containsIgnoreCase('.jpeg') && !AttchName.containsIgnoreCase('.png')){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select image file'));
            return null;
        }
		
        if(fileSize > 100000){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select image file less that 100 kb'));
            return null;
        }
        try {
            User currentuser=new User();
            currentuser=[Select Id,Name,Email,Signature__c from User where Id=:userinfo.getuserId()];
            
            List<Physician_Info__c> physicianInfo = new List<Physician_Info__c>();
            physicianInfo = [Select id, Name, Physician__c, SignatureURL__c, WatermarkImageURL__c, OwnerId From Physician_Info__c Where Physician__c=:userinfo.getuserId() limit 1];
            
            if(physicianInfo.size() < 1){
                Physician_Info__c physician =new Physician_Info__c();
                physician.Name = currentuser.Name;
                physician.Physician__c = currentuser.Id;
                insert physician;
                physicianInfo.add(physician);
            }
            
            String fileName = 'Signature '+currentuser.Name; 
            List<Attachment> attachmentList = [SELECT Id from Attachment WHERE ParentId=:physicianInfo[0].Id AND Name=:fileName];
            accSign = new Attachment();
            accSign.ParentID = physicianInfo[0].Id;
            //accSign.ParentID = '005j000000BkJiU';	
            accSign.Body = AttchBody;
            accSign.contentType = 'image/png';
            accSign.Name = 'Signature '+currentuser.Name;
            accSign.OwnerId = UserInfo.getUserId();
            accSign.Description=AttchDesc;
            insert accSign;
            
            if(attachmentList.size() > 0){
                delete attachmentList; 
            }
            
            String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm();
            String signatureURL = sfdcURL+'/servlet/servlet.FileDownload?file=' + accSign.Id;
            
            //update signature URL on physicianInfo recod
            physicianInfo[0].SignatureURL__c = signatureURL;
            update physicianInfo;         
        }

        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getlineNumber()+' '+ex.getMessage()));
            return null;
            
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Signature uploaded successfully'));
        PageReference showImage = new PageReference('/servlet/servlet.FileDownload?file=' + accSign.Id);
        showImage.setRedirect(true);
        return showImage;
        //return null;
    }

}