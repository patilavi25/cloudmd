public class ContactRecordController {
    public Contact contactRec {get; set;}    
    public String contactEmail {get; set;}
    
    public ContactRecordController(){
    	//contactRec = (Contact)contactRecords.getRecord();
        contactRec = new Contact();
        System.debug('Contact Rec:- '+contactRec);
    }
    
    public void getContact()
    {
        contactEmail = Apexpages.currentPage().getParameters().get('emailContact');
        System.debug('ContactEmail:---> '+contactEmail);
        System.debug('ContactEmail:---> ');
        Contact contactFetchedRec = [SELECT Id,Name,Email,FirstName,LastName,MobilePhone,Birthdate,Gender__c,
                   MailingCity,MailingState,MailingPostalCode,MailingStreet,MailingCountry,Reason_For_Visit__c
                   FROM Contact WHERE Email =: contactEmail LIMIT 1];
        System.debug('contactFetchedRec:---> '+contactFetchedRec.Name);
        contactRec.Email = contactFetchedRec.Email;
        contactRec.FirstName = contactFetchedRec.FirstName;
        contactRec.LastName = contactFetchedRec.LastName;
        contactRec.MobilePhone = contactFetchedRec.MobilePhone;
        contactRec.Birthdate = contactFetchedRec.Birthdate;
        contactRec.Gender__c = contactFetchedRec.Gender__c;
        contactRec.MailingCity = contactFetchedRec.MailingCity;
        contactRec.MailingState = contactFetchedRec.MailingState;
        contactRec.MailingPostalCode = contactFetchedRec.MailingPostalCode;
        contactRec.MailingCountry = contactFetchedRec.MailingCountry;
        contactRec.Reason_For_Visit__c = contactFetchedRec.Reason_For_Visit__c;
        
        System.debug('Contact --> '+contactRec.Name);
        //return null;
    }
    
    public PageReference Save(){
        try{
            insert contactRec;
        } catch(System.DMLException e){
            ApexPages.addMessages(e);
            return null;
        }
        PageReference redirectSuccess = new ApexPages.StandardController(contactRec).view();
        return (redirectSuccess);
    }
}