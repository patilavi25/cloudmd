@IsTest
public class Test_SavePatientsHangoutURLMapping {
	public static testmethod void HangoutUrlPosting() {

        //Create a Patient
        Contact p = new Contact();
        p.LastName = 'Patient';
        insert p ;
		
        SavePatientsHangoutURLMapping controller = new SavePatientsHangoutURLMapping();	    
        PageReference hPage = Page.HangoutAdvertisementPage;
        hPage.getParameters().put('LaunchForOppId',p.Id);
        hPage.getParameters().put('HangoutURLAsURLParameter','testurl');
        hPage.getParameters().put('PassedConsultationId','121');
        test.setCurrentPage(hPage);        
        controller.SaveMappig();
    }
}