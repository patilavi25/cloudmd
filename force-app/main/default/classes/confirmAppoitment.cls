public class confirmAppoitment { 
    //public string key = 'aEjalHsgTlmZRvbqH4BttQ' ;
    //public string secret = 'Zrkv9OieYN08RQvQq9ZxiDPB9UU40CX9';
    //public string key = 'jTcWLwfvQVGRCS4PdpXITg' ;
    //public string secret = 'YTqKIdj2qS3T7lC4uyOSMblZy9p45odY';
    public string key { get; set; }
    public string secret { get; set; }
    public string redirect_uri = 'https://'+ System.URL.getSalesforceBaseUrl().getHost()+'/apex/confirmAppointments';
    public string paramvalue='';  
    public String identificationToken ;    
    public String access_token;
    public static String consultingId { get; set; }
    Map<ID, Consulting__c> consultingMap {get; set;}
    
    public string meetingTitle{get;set;}
    public datetime startDateTime{get;set;}
    public integer meetingDuration{get;set;}
    public integer approvalType{get;set;}
    public string meetingPassword{get;set;}
    public Zoom_MeetingObject meetingObj{get;set;}
    public List<Consulting__c> consultingList {get;set;}
    public List<zoom_keys__mdt> zoomkeys {get;set;}
    
    
    public confirmAppoitment()
    {  
        zoomkeys = [Select Client_ID__c, Client_Secret__c, DeveloperName From zoom_keys__mdt Where DeveloperName = 'confirm_appointment']; 
        if(zoomkeys.size()>0){
            key = zoomkeys[0].Client_ID__c;
            secret = zoomkeys[0].Client_Secret__c;
        }
        consultingMap = new Map<ID, Consulting__c>([Select id, Name, Zoom_Meeting_UUID__c, From_Time__c, Zoom_Meeting_Password__c, Patients_Email__c, Visit__r.Patient__c, Patient__c, To_Time__c, User__c, User__r.Name, User__r.Email, User__r.TimeZoneSidKey, Visit__c, HangoutURL__c, isConfirmed__c From Consulting__c where isConfirmed__c = false AND Start_Time__c >= today AND Visit__c != null]); 
        //consultingList = [Select id, Name, From_Time__c, Visit__r.Patient__c, Patient__c, To_Time__c, User__c, Visit__c, HangoutURL__c, isConfirmed__c From Consulting__c where isConfirmed__c = false AND Visit__c != null]; 
        consultingList = consultingMap.values();
        meetingTitle = '';
        startDateTime = datetime.now();
        meetingDuration = 30;
        approvalType=0;
        meetingPassword = '';
        identificationToken = ApexPages.currentPage().getParameters().get('code') ; 
        system.debug('--identificationToken--'+identificationToken);
        if(identificationToken != '' && identificationToken != null)
        {
            AccessToken();
        } 
    }
    
    public PageReference ZoomAuth()
    {
        PageReference pg = new PageReference(new Zoom_AuthURIAfterRequest(key , redirect_uri).AuthenticationURI) ;
        return pg;
        
    }
    
    public void AccessToken()
    {
        system.debug('==code=='+identificationToken);
        paramvalue = System.EncodingUtil.base64Encode(Blob.valueof(key+':'+secret));
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('https://zoom.us/oauth/token');
        String messageBody = 'grant_type=authorization_code&code='+identificationToken+'&redirect_uri='+redirect_uri;
        req.setHeader('Authorization', 'Basic '+paramvalue);
        req.setBody(messageBody);
        req.setTimeout(60*1000);
        
        Http h = new Http();
        HttpResponse res = h.send(req);
        system.debug(res.getBody());
        JSONParser parser = JSON.createParser(res.getBody());
        system.debug('Pradeep test >> '+parser);
        while (parser.nextToken() != null) 
        {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token'))
            {
                parser.nextToken();
                access_token=parser.getText();
                break;
            }
        }
        if (access_token!='' && access_token != null)
        {system.debug('Pradeep access_token >> '+access_token);
         //getUserId();
         //setMeeting();
        }
        
    }
    
    public PageReference getUserId()
    {  
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('GET');
        req1.setEndpoint('https://api.zoom.us/v2/users?page_number=1&page_size=30&status=active');
        req1.setHeader('authorization', 'Bearer '+access_token);
        req1.setHeader('content-type', 'application/json');
        
        req1.setTimeout(60*1000);
        Http h2 = new Http();
        HttpResponse resl = h2.send(req1);
        System.debug('User List Pradeep'+resl.getBody());
        
        return null;
    }
    
    public PageReference setMeeting()
    {  
        System.debug('--Testing--');
        system.debug('--access_token--'+access_token);
        //Consulting__c c = [Select id, Name, From_Time__c, Zoom_Meeting_Password__c, To_Time__c, Patients_Email__c, Visit__r.Patient__c, Patient__c, User__c, User__r.Name, Visit__c, HangoutURL__c, isConfirmed__c From Consulting__c where Id =: consultingId];     
        Consulting__c c = consultingMap.get(consultingId);
        String Physicianstimezone = c.User__r.TimeZoneSidKey;
        meetingTitle = 'Appointment with Doctor '+ c.User__r.Name;
        meetingPassword = String.valueOf(Math.round(Math.random()*30000));
        startDateTime = System.now();
        system.debug('--meetingTitle--'+meetingTitle+'--c.From_Time__c--'+c.From_Time__c);
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('POST');
        req1.setEndpoint('https://api.zoom.us/v2/users/avinash.patil@bolt.today/meetings');
        req1.setHeader('content-type', 'application/json');
        req1.setHeader('Authorization', 'Bearer '+access_token);
        String messageBody = '{ \"topic\": \"'+meetingTitle+'\", \"schedule_for\": \"'+c.User__r.Email+'\", \"start_time\": \"'+c.From_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss.SS\'Z\'')+'\", \"duration\": '+meetingDuration+',  \"timezone\": \"'+Physicianstimezone+'\", \"password\": \"'+meetingPassword+'\", \"settings\": {\"approval_type\": '+approvalType+', \"auto_recording\": \"cloud\",\"contact_email\": \"'+c.Patients_Email__c+'\",\"registrants_email_notification\": \"true\"}}';
        //String messageBody = '{ \"topic\": \"'+meetingTitle+'\", \"schedule_for\": \"'+c.User__r.Email+'\", \"start_time\": \"'+c.From_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss.SS\'Z\'')+'\", \"duration\": '+meetingDuration+',  \"timezone\": \"'+Physicianstimezone+'\", \"password\": \"'+meetingPassword+'\"}';
        System.debug('messageBody Pradeep'+messageBody);
        req1.setBody(messageBody);
        req1.setTimeout(60*1000);
        Http h2 = new Http();
        HttpResponse resl = h2.send(req1);
        System.debug('Meeting Pradeep'+resl.getBody());
        system.debug('Response code: '+resl.getStatusCode());
        JSONParser parserD = JSON.createParser(resl.getBody());
        Integer statusCode  = resl.getStatusCode();
        system.debug('Pradeep parserD >> '+ parserD);
        
        meetingObj = Zoom_MeetingObject.parse( resl.getBody());
        system.debug('Meeting info >> '+meetingObj);
        
        if(statusCode == 201){
            c.HangoutURL__c = meetingObj.join_url;
            c.isConfirmed__c = true;
            c.meetingId__c =  String.valueOf(meetingObj.id);
            c.Zoom_Meeting_Password__c = meetingPassword;
            String MeetingUUID = getMeetingUUID(String.valueOf(meetingObj.id));
            System.debug('--MeetingUUID--'+MeetingUUID);
            c.Zoom_Meeting_UUID__c = MeetingUUID;
            update c;
            sendMail(c); 
            consultingMap.remove(c.Id);
            consultingList = consultingMap.values();
            //consultingList = [Select id, Name, From_Time__c, To_Time__c, Visit__r.Patient__c, Patient__c, User__c, Visit__c, HangoutURL__c, isConfirmed__c From Consulting__c where isConfirmed__c = false AND Visit__c != null]; 
        }
        
        return null;
    }
    
    public static void sendMail(Consulting__c c){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        Date d = c.From_Time__c.date();
        system.debug(d);
        String date1 = Datetime.newInstance(d.year(), d.month(), d.day()).format('dd-MMM-yyyy');
        List<String> sendTo = new List<String>();
        sendTo.add(c.Patients_Email__c);
        List<String> sendCC = new List<String>();
        sendCC.add(c.User__r.Email);
        email.setToAddresses(sendTo);
        email.setCcAddresses(sendCC);
        List<OrgWideEmailAddress> lstEmailAddress=[select Id from OrgWideEmailAddress WHERE Address='info@cloudmdfoundation.org'];       
        // Set Organization-Wide Email Address Id
        email.setOrgWideEmailAddressId(lstEmailAddress[0].Id); 
        email.setSubject('Appointment with Doctor '+ c.User__r.Name +' has been Confirmed');
        
        String messageBody = '<html><head><style>table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}</style></head><body>Hello <b>'+c.Patient__c+',</b></br></br>Just a friendly reminder about our upcoming appointment:</br></br><table><tr><td>Doctors name </td><td> <b>'+c.User__r.Name+'</b> </td></tr><tr><td>Date and Time </td><td><b>'+date1+', '+c.From_Time__c.format('h:mm a')+' ('+c.User__r.TimeZoneSidKey + ')</b></td></tr><tr><td>Meeting Link</td><td><b>'+c.HangoutURL__c+'</b></td></tr><tr><td>Meeting Password</td><td><b>'+c.Zoom_Meeting_Password__c+'</b></td></tr></table></body></html>';       
        email.setHtmlBody(messageBody);
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }catch(exception e){
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getMessage()));
        }

    }   
    
    public String getMeetingUUID(String meetingId){
        
        //Consulting__c c = consultingMap.get(consultingId);
        system.debug('**Access Token**'+access_token);
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('GET');
        req1.setEndpoint('https://api.zoom.us/v2/meetings/'+meetingId);
        //req1.setEndpoint('https://api.zoom.us/v2/meetings/93026049290');
        req1.setHeader('content-type', 'application/json');
        req1.setHeader('Authorization', 'Bearer '+access_token);
        req1.setTimeout(60*1000);
        Http h2 = new Http();
        HttpResponse resl = h2.send(req1);
        JSONParser parserD = JSON.createParser(resl.getBody());
        system.debug('--response--'+resl+'--'+resl.getStatus());
        Integer statusCode  = resl.getStatusCode();
        system.debug('Pradeep parserD >> '+ parserD);
        
        Zoom_MeetingObject meetingObj1 = Zoom_MeetingObject.parse( resl.getBody());
        system.debug('Meeting info >> '+meetingObj1);
        system.debug('--meetingObj1 uuid--'+meetingObj1.uuid);
        return meetingObj1.uuid;   
    }
}