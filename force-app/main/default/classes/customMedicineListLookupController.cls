public class customMedicineListLookupController {
    
    public Medicine_List__c ml {get;set;} // new account to create
    public List<Medicine_List__c> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    
    public customMedicineListLookupController() {
        ml = new Medicine_List__c();
        // get the current search string
        searchString = System.currentPageReference().getParameters().get('lksrch');
        runSearch();  
    }
    
    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }
    
    // prepare the query and issue the search command
    private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        results = performSearch(searchString);               
    } 
    
    // run the search and return the records found. 
    private List<Medicine_List__c> performSearch(string searchString) {
        
        String soql = 'select id, name, Strength__c, Formulation__c from Medicine_List__c';
        if(searchString != '' && searchString != null)
            soql = soql +  ' where Name LIKE \'%' + searchString +'%\'';
        soql = soql + ' limit 100';
        System.debug(soql);
        return database.query(soql); 
        
    }
    
    // save the new Medicine List record
    public PageReference saveMedicineList() {
        try{
            insert ml;
            results.add(ml);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'record created successfully, please click search'));
        }catch(DmlException e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));      
        }  
        // reset the Medicine List
        ml = new Medicine_List__c();
        return null;
    }
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    
}