public class newPatientMedicinesController {
    
    public Patient_Medicine__c pm {get;set;}
    
    public newPatientMedicinesController(ApexPages.StandardController stdController) {
        if(pm == null){
            pm = new Patient_Medicine__c();
            pm.Patient__c = apexpages.currentpage().getparameters().get('contactId');
        }
    }
    
    public PageReference  save(){
        insert pm;
        PageReference orderPage = new PageReference('/' + pm.Patient__c);
        orderPage.setRedirect(true);
        return orderPage;
    }
    public PageReference  Cancel(){
        PageReference orderPage = new PageReference('/' + pm.Patient__c);
        orderPage.setRedirect(true);
        return orderPage;
    }
    
}