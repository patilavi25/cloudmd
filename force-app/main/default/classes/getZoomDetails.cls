public class getZoomDetails {
 	public string key = 'kMPzDLh2QNmesFnYoQvipw' ;
    public string secret = 'vmkcTyxZsLWou6cFMcJ41JInniZmfPti';
    //public string key = 'XQpC2QQSRNa9xhh1PGFTaQ' ;
    //public string secret = 'H9vzHLllO079rUQfopLDiMZoolJ6XNYH';
    public string redirect_uri = 'https://'+ System.URL.getSalesforceBaseUrl().getHost()+'/apex/getMeetingDetails';
    public string paramvalue='';  
    public String identificationToken ;    
    public String access_token;
    public static String consultingId { get; set; }
    Map<ID, Consulting__c> consultingMap {get; set;}
    
    public string meetingTitle{get;set;}
    public datetime startDateTime{get;set;}
    public integer meetingDuration{get;set;}
    public integer approvalType{get;set;}
    public string meetingPassword{get;set;}
    public Zoom_MeetingObject meetingObj{get;set;}
    public List<Consulting__c> consultingList {get;set;}
    
    
    
    public getZoomDetails()
    {  
        consultingMap = new Map<ID, Consulting__c>([Select id, Name, meetingId__c, Meeting_details_updated__c, Zoom_Meeting_UUID__c, From_Time__c, Zoom_Meeting_Password__c, Patients_Email__c, Visit__r.Patient__c, Patient__c, To_Time__c, User__c, User__r.Name, User__r.Email, User__r.TimeZoneSidKey, Visit__c, HangoutURL__c, isConfirmed__c From Consulting__c where isConfirmed__c = True AND Visit__c != null AND Start_Time__c <= today AND Meeting_details_updated__c = false]); 
        //consultingList = [Select id, Name, From_Time__c, Visit__r.Patient__c, Patient__c, To_Time__c, User__c, Visit__c, HangoutURL__c, isConfirmed__c From Consulting__c where isConfirmed__c = false AND Visit__c != null]; 
        consultingList = consultingMap.values();
        meetingTitle = '';
        startDateTime = datetime.now();
        meetingDuration = 30;
        approvalType=0;
        meetingPassword = '';
        identificationToken = ApexPages.currentPage().getParameters().get('code') ; 
        system.debug('--identificationToken--'+identificationToken);
        if(identificationToken != '' && identificationToken != null)
        {
            AccessToken();
        } 
    }
    
    public PageReference ZoomAuth()
    {
        PageReference pg = new PageReference(new Zoom_AuthURIAfterRequest(key , redirect_uri).AuthenticationURI) ;
        return pg;
        
    }
    
    public void AccessToken()
    {
        system.debug('==code=='+identificationToken);
        paramvalue = System.EncodingUtil.base64Encode(Blob.valueof(key+':'+secret));
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('https://zoom.us/oauth/token');
        String messageBody = 'grant_type=authorization_code&code='+identificationToken+'&redirect_uri='+redirect_uri;
        req.setHeader('Authorization', 'Basic '+paramvalue);
        req.setBody(messageBody);
        req.setTimeout(60*1000);
        
        Http h = new Http();
        HttpResponse res = h.send(req);
        system.debug(res.getBody());
        JSONParser parser = JSON.createParser(res.getBody());
        system.debug('Pradeep test >> '+parser);
        while (parser.nextToken() != null) 
        {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token'))
            {
                parser.nextToken();
                access_token=parser.getText();
                break;
            }
        }
        if (access_token!='' && access_token != null)
        {system.debug('Pradeep access_token >> '+access_token);
         //getUserId();
         //setMeeting();
        }
        
    }
    
    public PageReference getUserId()
    {  
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('GET');
        req1.setEndpoint('https://api.zoom.us/v2/users?page_number=1&page_size=30&status=active');
        req1.setHeader('authorization', 'Bearer '+access_token);
        req1.setHeader('content-type', 'application/json');
        
        req1.setTimeout(60*1000);
        Http h2 = new Http();
        HttpResponse resl = h2.send(req1);
        System.debug('User List Pradeep'+resl.getBody());
        
        return null;
    }
    
    public String getMeetingUUID(){
        
        Consulting__c c = consultingMap.get(consultingId);
        system.debug('**Access Token**'+access_token);
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('GET');
        //req1.setEndpoint('https://api.zoom.us/v2/past_meetings/KtvONKn6Tqat43IZ3PcSAA==');
        req1.setEndpoint('https://api.zoom.us/v2/meetings/'+c.meetingId__c);
        //req1.setEndpoint('https://api.zoom.us/v2/meetings/93026049290');
        req1.setHeader('content-type', 'application/json');
        req1.setHeader('Authorization', 'Bearer '+access_token);
        //String messageBody = '{ \"topic\": \"'+meetingTitle+'\", \"schedule_for\": \"'+c.User__r.Email+'\", \"start_time\": \"'+c.From_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss.SS\'Z\'')+'\", \"duration\": '+meetingDuration+',  \"timezone\": \"'+Physicianstimezone+'\", \"password\": \"'+meetingPassword+'\"}';
        //System.debug('messageBody Pradeep'+messageBody);
        //req1.setBody(messageBody);
        req1.setTimeout(60*1000);
        Http h2 = new Http();
        HttpResponse resl = h2.send(req1);
        System.debug('Meeting Pradeep'+resl.getBody());
        system.debug('Response code: '+resl.getStatusCode());
        JSONParser parserD = JSON.createParser(resl.getBody());
        system.debug('--response--'+resl+'--'+resl.getStatus());
        Integer statusCode  = resl.getStatusCode();
        system.debug('Pradeep parserD >> '+ parserD);
        
        meetingObj = Zoom_MeetingObject.parse( resl.getBody());
        system.debug('Meeting info >> '+meetingObj);
        system.debug('--meetingObj uuid--'+meetingObj.uuid);
        return meetingObj.uuid;   
    }  
    
    public PageReference getMeetingDetails(){
		
        Consulting__c c = consultingMap.get(consultingId);
        //String meetingUUid = getMeetingUUID();
        system.debug('--In getMeetingDetails Method--'+access_token);
        //system.debug('--getMeetingUUID--'+meetingUUid);
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('GET');
        req1.setEndpoint('https://api.zoom.us/v2/past_meetings/'+c.Zoom_Meeting_UUID__c);
        //req1.setEndpoint('https://api.zoom.us/v2/past_meetings/KtvONKn6Tqat43IZ3PcSAA==');
        //https://api.zoom.us/v2/past_meetings/KtvONKn6Tqat43IZ3PcSAA==
        //req1.setEndpoint('https://api.zoom.us/v2/meetings/93650752067');
        req1.setHeader('content-type', 'application/json');
        req1.setHeader('Authorization', 'Bearer '+access_token);
        req1.setTimeout(60*1000);
        Http h2 = new Http();
        HttpResponse resl = h2.send(req1);
        System.debug('Meeting Pradeep1'+resl.getBody());
        system.debug('Response code1: '+resl.getStatusCode());
        JSONParser parserD = JSON.createParser(resl.getBody());
        system.debug('--response--'+resl.getStatus());
        Integer statusCode  = resl.getStatusCode();
        system.debug('Pradeep parserD 1>> '+ parserD);
        
        meetingObj = Zoom_MeetingObject.parse(resl.getBody());
        system.debug('Meeting info >> '+meetingObj);
        system.debug('--meetingObj uuid--1'+meetingObj.uuid);
        if(meetingObj.uuid == null){
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Meeting does not happend'));
        }else{
            
            c.Meeting_duration_min__c = meetingObj.duration;
            c.Meeting_details_updated__c = true;
            update c;
            consultingMap.remove(c.Id);
            consultingList = consultingMap.values();
        }
        return null;
    }
}