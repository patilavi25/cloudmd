@IsTest
public class Test_PatientSummary {
    public static testmethod void Summary_Test() {
        //Create a Patient
        Contact p = new Contact();
        p.LastName = 'Patient';
        p.Gender__c = 'Male';
        p.Birthdate = system.today();
        insert p ;
        
        //Create a Visit
        Visit__c v = new Visit__c (Check_In_time__c = system.now() + 100, Onsite_Physician__c = UserInfo.getUserId(), Patient__c = p.Id, 
                                   ReasonforVisit__c = 'Sick');
        insert v;
		
        //Create a SOAP Note
        SOAP_Note__c sn = new SOAP_Note__c (Soap_Assessment__c = 's', Soap_Objective__c = 's', Soap_Plan__c = 's', Subjective__c = 's', Visit__c = v.Id);
        insert sn;
    
        //Create Availabilities for Remote Physician
        Availability__c av = new Availability__c(User__c = UserInfo.getUserId(), Start_Date__c = system.today(), End_Date__c = system.today()+1, 
                                                 From_Time__c = '00:15 AM', To_Time__c = '11:30 PM', Sunday__c = True, Monday__c = True,
                                                 Tuesday__c = True, Wednesday__c = True, Thrusday__c = True, Friday__c = True, Saturday__c = True);
        insert av;
        
        //Query Slots
        List<Slot__c> slot = new List<Slot__c>([Select Id, Availability__c, Physician__c, Start_Time__c, End_Time__c, Is_Booked__c, Is_Future_Slot__c, Cancelled__c  from Slot__c order by End_Time__c Desc limit 1]);
        system.debug('Slots - '+slot);
        //Create Consulting Record
        Consulting__c con = new Consulting__c(User__c = UserInfo.getUserId(), Reason__c = 'Sick', Slot__c = slot[0].Id );
        insert con;
        
        //Test the Controller.
        PatientSummary controller = new PatientSummary();
        controller.getAllergies();
        controller.getDiagnosis();
        controller.getlabReport();
        controller.getMedication();
        controller.getProblemList();
        controller.getSOAPNote();
        controller.getSOAPNotes();
        controller.getVitalRecord();
        controller.showPopup();
        
        //Create a new instance of standard controller
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(p);
        PageReference psPage = Page.PatientSummaryPage;
        psPage.getParameters().put('Id',p.Id);
        test.setCurrentPage(psPage);        
        PatientSummary psc = new PatientSummary(sc);
        psc.selectedSOAPId = sn.Id;
		psc.fetchSOAPRecord();
        psc.getSOAPNote();
        psc.saveSOAPNotes();
        psc.closePopup();
        psc.closeAddPopup();
        psc.addPopup();
        psc.saveDiagonosisPopup();
    }
}