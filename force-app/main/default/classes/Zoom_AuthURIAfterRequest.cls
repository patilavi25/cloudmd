global class Zoom_AuthURIAfterRequest
{
    global String AuthenticationURI='';
    public Zoom_AuthURIAfterRequest(String Clientkey,String redirect_uri)
    {
        String key = EncodingUtil.urlEncode(Clientkey,'UTF-8');
        String uri = EncodingUtil.urlEncode(redirect_uri,'UTF-8');
        
         String  authuri = 'https://zoom.us/oauth/authorize?response_type=code&client_id='+Clientkey+'&redirect_uri='+redirect_uri;
        AuthenticationURI=authuri;
    }
}