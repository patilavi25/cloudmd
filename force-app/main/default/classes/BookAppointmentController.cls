global class BookAppointmentController {
    
    //public String selectedvalue{get;set;}
    public Static String selectedvalue {get; set;}
    public String selectedslot {get; set;}
    public Static List<jsonWrapper> jsonWrapperList {set;get;}
    public Static String finalJSON {get; set;}
    public Static Set<String> dateSet {set;get;}
    public Static List<SelectOption> slotsOptions1 {set;get;}
    global Static List<SelectOption> slotsOptions {set;get;}
    public static boolean displayAddPopup {get; set;}
    Map<id,String> physicianMap {get; set;}
    
    public class jsonWrapper{
        public String start;
        public String end1;
        public String color;
        public String backgroundColor;
        public Boolean overlap;
        public String rendering;
        
        public jsonWrapper(String startDate, String endDate){
            this.start = startDate;
            this.end1 = endDate;
            color = 'Green';
            backgroundColor = 'Green';
            overlap = false;
            rendering = 'background';
        }
    }
    
    public BookAppointmentController(){
        displayAddPopup = True;
        slotsOptions1 = new List<SelectOption>();
        physicianMap = new Map<id,String>();
        //slotsOptions = new List<SelectOption>();
        //availableSlots1('2020-04-24','005j000000BkJiUAAV');
        //selectedvalue = new String();
        //jsonWrapperList = new List<jsonWrapper>();
    }
    
    public List<SelectOption> getPhysician() {
        List<SelectOption> options = new List<SelectOption>();
        List<Profile> profileIds = [Select Id, name From Profile Where Name In ('System Administrator', 'Remote Physician', 'Onsite Physician')];
        List<User> physicianList = [Select Id, Name, Doctor_Code__c From User where ProfileId IN : profileIds AND isActive = true AND Doctor_Code__c !=null Order by Doctor_Code__c];
        options.add(new SelectOption('','--Select Physician--'));
        for(User u: physicianList){
            options.add(new SelectOption(u.Id,u.Doctor_Code__c));
            physicianMap.put(u.Id,u.Name);
        }
        //getavailableSlots2();
        return options;   
    }
    
    public void checkSlots(){
        jsonWrapperList = new List<jsonWrapper>();
        jsonWrapperList.clear();
        dateSet = new Set<String>();
        Set<Date> setD = new Set<Date>();
        //system.debug('--slotsOptions for picklist--'+slotsOptions1);
        finalJSON = '';
        system.debug('--selectedvalue--'+selectedvalue);
        List<Slot__C> slotsLst = [select id, Start_Time__c, End_Time__c, Availability__r.User__c, Availability__c, Is_Booked__c, Meeting_Date__c From Slot__c Where Meeting_Date__c >= today AND Is_Booked__c = false AND Availability__r.User__c =: selectedvalue Order by Meeting_Date__c ASC];
        system.debug('--slotsLst--'+slotsLst.size());
        if(slotsLst.size()>0){
            for(Slot__c s : slotsLst){    
                dateSet.add(string.valueof(s.Meeting_Date__c).SubStringBefore(' '));
                setD.add(s.Meeting_Date__c);
            }
            system.debug('--dateSet--'+dateSet);
            Date start;
            Date incrementalDate;
            Date current;
            integer intt = 0;
            for(String d: dateSet){
                intt = intt+1;
                if(incrementalDate==null){
                    start=date.valueOf(d);
                    incrementalDate = date.valueOf(d);
                }
                current=date.valueOf(d);
                system.debug('--start--'+start+'--current--'+current);
                Integer diff = incrementalDate.daysBetween(current);
                system.debug('--Date diff--'+diff);
                if(incrementalDate.daysBetween(current)==0 || incrementalDate.daysBetween(current)==1){
                    system.debug('--start--'+start+'--current--'+current);
                    incrementalDate = current;
                    if(dateSet.size()==intt){
                        jsonWrapperList.add(new jsonWrapper(string.valueof(start).SubStringBefore(' '),string.valueof(incrementalDate+1).SubStringBefore(' ')));                    
                    }
                    continue;
                    
                }else{
                    jsonWrapperList.add(new jsonWrapper(string.valueof(start).SubStringBefore(' '),string.valueof(incrementalDate+1).SubStringBefore(' ')));
                    //jsonWrapperList.add(new jsonWrapper(string.valueof(start).SubStringBefore(' '),string.valueof(incrementalDate).SubStringBefore(' ')));
                    start=current;
                    incrementalDate=current;
                }
                
                /*jsonWrapperList.add(new jsonWrapper(d,d));*/
            }
        }
        if(jsonWrapperList.size()>0){
            finalJSON = JSON.serialize(jsonWrapperList);
            finalJSON = finalJSON.replace('end1','end');
            system.debug('----finalJSON----'+finalJSON);
        }
    }
    
    @RemoteAction
    public Static Map<Id,String> availableSlots(String selecteddate, String selectedUser){
        slotsOptions = new List<SelectOption>();
        Map<Id,String> slotMap = new Map<Id,String>();
        system.debug('--selecteddate--'+selecteddate+'--selectedUser--'+selectedUser);
        List<Slot__C> slotsLst = [select id, Start_Time__c, End_Time__c, StartDateText__c, Meeting_Date__c, Is_Booked__c From Slot__c Where StartDateText__c =: selecteddate AND Is_Booked__c = false AND Availability__r.User__c =: selectedUser Order by Start_Time__c ASC];
        system.debug('--slotsLst--'+slotsLst);
        if(slotsLst.size()>0){
            for(Slot__C slot : slotsLst){  
                //String slotName = slot.Start_Time__c.format('MMMM dd, yyyy') + ', From: '+slot.Start_Time__c.format('h:mm a')+' To: '+slot.End_Time__c.format('h:mm a');
                String slotName = slot.Start_Time__c.format('h:mm a');
                slotsOptions.add(new SelectOption(slot.Id,slotName));
                slotMap.put(slot.Id,slotName);
            }  
        }
        system.debug('--slotsOptions--'+slotsOptions);
        displayAddPopup = true; 
        system.debug('--displayAddPopup--'+displayAddPopup);
        //slotsOptions1 = slotsOptions;
        //getavailableSlots1();
        //this.slotsOptions = slotsOptions;
        return slotMap;
    }
    
    public PageReference createAppointment(){
        String fname=Apexpages.currentPage().getParameters().get('fname');
        String lname=Apexpages.currentPage().getParameters().get('lname'); 
        String email=Apexpages.currentPage().getParameters().get('email'); 
        String descr =Apexpages.currentPage().getParameters().get('descr'); 
        //String input1=Apexpages.currentPage().getParameters().get('input1'); 
        String SlotId=Apexpages.currentPage().getParameters().get('SlotId');
        String phoneNumber=Apexpages.currentPage().getParameters().get('phone');
        
        try{
            List<Contact> availableCon = [Select Id, FirstName, AccountId, LastName, Phone, Name, Email From Contact where Email=:email Limit 1];
            List<Account> availableAcc = [Select Id, Name From Account where Name= 'BGD-DCI-Dhaka' Limit 1];            
            List<Slot__C> selectedslot = [select id, Start_Time__c, End_Time__c, StartDateText__c, Meeting_Date__c From Slot__c Where Id =: SlotId limit 1];
            system.debug('==Entered the Controller: '+ fname+lname+email+descr+slotid );
            system.debug('selectedvalue--'+selectedvalue+'--selectedSlot--'+SlotId);
          	User SelectedPhysician = [Select FirstName, LastName, Name From User where Id=:selectedvalue];
            system.debug('--availableAcc--'+availableAcc);
            Contact con;
            //create patient record
            if(availableCon.size()>0){
                con = availableCon[0];
                if(availableAcc.size()>0){
                  con.AccountId = availableAcc[0].Id;
                }
                con.FirstName = fname;
                con.LastName = lname;
                con.email = email;
                con.Phone = phoneNumber;
                update con;
            }else{
                con = new Contact();
                if(availableAcc.size()>0){
                  con.AccountId = availableAcc[0].Id;
                }
                con.FirstName = fname;
                con.LastName = lname;
                con.email = email;
                con.Phone = phoneNumber;
                insert con;
            }            
			
			system.debug('--con--'+con);            
            //create appointment
            Visit__c v = new Visit__c();
            v.Onsite_Physician__c = selectedvalue;
            v.Patient__c = con.Id;
            v.ReasonforVisit__c = descr;
            v.Check_In_time__c = selectedslot[0].Start_Time__c;
            insert v;
            
            //create Consulting record
            Consulting__c conslt = new Consulting__c();
            conslt.Visit__c = v.Id;
            conslt.Slot__c= selectedslot[0].Id;
            conslt.Start_Time__c= selectedslot[0].Start_Time__c;
            conslt.End_Time__c= selectedslot[0].End_Time__c;
            conslt.User__c = selectedvalue;
            conslt.Reason__c = descr;
            conslt.Patients_Email__c = email;
            //conslt.HangoutURL__c = 'https://hangouts.google.com/call/QezTN3YVBi7x8iKuPELsAEEE';
            insert conslt;
            
            selectedslot[0].Is_Booked__c = true;
            update selectedslot;

            //----Send Email----
            
            Messaging.SingleEmailMessage emailToPatient = new Messaging.SingleEmailMessage();
            Date d = conslt.Start_Time__c.date();
            system.debug('D: '+d);
            String date1 = Datetime.newInstance(d.year(), d.month(), d.day()).format('dd-MMM-yyyy');
            List<String> sendTo = new List<String>();
            sendTo.add(conslt.Patients_Email__c);
            List<String> sendCC = new List<String>();
            sendCC.add(conslt.User__r.Email);
            List<String> sendBCC = new List<String>();
            emailToPatient.setToAddresses(sendTo);
            //emailToPatient.setCcAddresses(sendCC);
            List<OrgWideEmailAddress> lstEmailAddress=[select Id from OrgWideEmailAddress WHERE Address='info@cloudmdfoundation.org'];       
            // Set Organization-Wide Email Address Id
            emailToPatient.setOrgWideEmailAddressId(lstEmailAddress[0].Id); 
            emailToPatient.setSubject('Your appointment has been booked with Doctor '+ SelectedPhysician.Name);
            
            String messageBody = '<html><head><style>table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}</style></head><body>Hello <b>'+con.Name+', </b></br></br>Your appoitment has been booked successfully for date ' +date1+' & time '+ conslt.Start_Time__c.format('h:mm a') +'. You will receive meeting details once doctor confirms the appointment</body></html>';
            emailToPatient.setHtmlBody(messageBody);
            
            List<String> sendToAdmin = new List<String>();
            sendToAdmin.add('patilavi25@gmail.com');
            //Email to Admin
            Messaging.SingleEmailMessage emailToAdmin = new Messaging.SingleEmailMessage();
            emailToAdmin.setToAddresses(sendToAdmin);
            emailToAdmin.setOrgWideEmailAddressId(lstEmailAddress[0].Id); 
            emailToAdmin.setSubject('Appointment requested by ' + con.Name);
            String messageBody2 = '<html><head><style>table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}</style></head><body>Hello,</br></br></br> Appoitment has been requested by '+con.Name+' for date ' +date1+' & time '+ conslt.Start_Time__c.format('h:mm a') +'.</body></html>';
            emailToAdmin.setHtmlBody(messageBody2);
            system.debug('send emails: ');
            
            try{
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { emailToPatient });
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { emailToAdmin });
            }catch(exception e){
                system.debug('error occured in sending emails: '+e.getMessage());
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getMessage()));
            }
            
            //----send Email----
            
            
            
            //Page.BookAppointmentSuccess.setRedirect(true);
            PageReference pageRef = new PageReference('/apex/BookAppointmentSuccess');
            pageRef.getParameters().put('Date',selectedslot[0].Start_Time__c.format('MMMM dd, yyyy'));
            pageRef.getParameters().put('From',selectedslot[0].Start_Time__c.format('h:mm a'));
            pageRef.getParameters().put('To',selectedslot[0].End_Time__c.format('h:mm a'));
            return pageRef;
            
        }catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            return null;
        }   
        //return null;
    }
    
    public void closeAddPopup() { 
        //system.debug('displayAddPopup '+ displayAddPopup);
    	displayAddPopup = false;
        //render_on_add = false;
        //diagnos = null;
    }
    
    public PageReference closePopup() { 
        system.debug('displayAddPopup '+ displayAddPopup);
    	displayAddPopup = false;
        system.debug('--displayAddPopup--'+displayAddPopup);
        //render_on_add = false;
        //diagnos = null;
        return null;
    }
    
    public static List<SelectOption> getavailableSlots1(){
        system.debug('--slotsOptions for picklist--'+slotsOptions1);
        //slotsOptions.add(new SelectOption('','--select slot--'));
        return slotsOptions;
    }
    
    public static void sendMail(Consulting__c c){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        Date d = c.Start_Time__c.date();
        system.debug('D: '+d);
        String date1 = Datetime.newInstance(d.year(), d.month(), d.day()).format('dd-MMM-yyyy');
        List<String> sendTo = new List<String>();
        sendTo.add(c.Patients_Email__c);
        List<String> sendCC = new List<String>();
        sendCC.add(c.User__r.Email);
        List<String> sendBCC = new List<String>();
        email.setToAddresses(sendTo);
        //email.setCcAddresses(sendCC);
        List<OrgWideEmailAddress> lstEmailAddress=[select Id from OrgWideEmailAddress WHERE Address='info@cloudmdfoundation.org'];       
        // Set Organization-Wide Email Address Id
        email.setOrgWideEmailAddressId(lstEmailAddress[0].Id); 
        email.setSubject('Your appointment has been booked with Doctor '+ c.User__r.Name);
        
        String messageBody = '<html><head><style>table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}</style></head><body>Hello <b>'+c.Patient__c+',</b></br></br>Your appoitment has been booked successfully for date ' +c.Start_Time__c.format('h:mm a') +' You will receive meeting details once doctor confirms the appointment</body></html>';
        email.setHtmlBody(messageBody);
        
        List<String> sendToAdmin = new List<String>();
        sendToAdmin.add('patilavi25@gmail.com');
        //Email to Admin
        Messaging.SingleEmailMessage emailToAdmin = new Messaging.SingleEmailMessage();
        emailToAdmin.setToAddresses(sendToAdmin);
        emailToAdmin.setOrgWideEmailAddressId(lstEmailAddress[0].Id); 
		emailToAdmin.setSubject('Appointment requested by ' + c.Patient__c);
        String messageBody2 = '<html><head><style>table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}</style></head><body>Hello,</br></br></br> Appoitment has been requested by '+c.Patient__c+' for date ' +date1+', '+ c.Start_Time__c.format('h:mm a') +'.</body></html>';
        emailToAdmin.setHtmlBody(messageBody2);
        system.debug('send emails: ');

        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { emailToAdmin });
        }catch(exception e){
            system.debug('error occured in sending emails: '+e.getMessage());
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getMessage()));
        }
    }
}