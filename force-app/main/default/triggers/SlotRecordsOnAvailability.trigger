trigger SlotRecordsOnAvailability on Availability__c (After Insert) {
    AvailabilityTriggerHandler ATH = new AvailabilityTriggerHandler ();
    if(trigger.isInsert && trigger.isAfter) {
        ATH .afterInsert_Availability(trigger.new);
    }
}