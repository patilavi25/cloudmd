trigger triggerConsulting on Consulting__c (after insert, after update) {
    List<Slot__c> slots = new List<Slot__c>();
    Set<Id> otherSlotsToupdateIsBookedFree = new Set<Id>();
    for(Consulting__c con : trigger.new) {
        Slots.add(new Slot__c(Id = con.Slot__c, is_Booked__c = true ));
        if(trigger.isupdate) {
            if( trigger.oldmap.get(con.Id).Slot__c != con.Slot__c ) {
                otherSlotsToupdateIsBookedFree.add(trigger.oldmap.get(con.Id).Slot__c);
            }
        }
    }
    
    if(otherSlotsToupdateIsBookedFree.size() > 0 ) {
        for(Id slotId : otherSlotsToupdateIsBookedFree) {
            Slots.add(new Slot__c(Id = slotId , is_Booked__c = false));
        }
    }
    
    if(Slots.size() > 0 )
        update Slots; 
}